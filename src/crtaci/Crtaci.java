/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package crtaci;

import com.sun.org.apache.bcel.internal.generic.FLOAD;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.PerspectiveCamera;
import javafx.scene.PointLight;
import javafx.scene.Scene;
import javafx.scene.SceneAntialiasing;
import javafx.scene.control.Button;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.paint.PhongMaterial;
import javafx.scene.shape.Cylinder;
import javafx.scene.shape.DrawMode;
import javafx.scene.shape.MeshView;
import javafx.scene.shape.Rectangle;
import javafx.scene.shape.TriangleMesh;
import javafx.scene.transform.Rotate;
import javafx.scene.transform.Translate;
import javafx.stage.Stage;

/**
 *
 * @author marko.pekovic
 */
public class Crtaci extends Application {

    PerspectiveCamera kamera;

    @Override
    public void start(Stage primaryStage) {
        Button btn = new Button();

        btn.setText("Say 'Hello World'");
        btn.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                System.out.println("Hello World!");
            }
        });

        Cylinder rv = new Cylinder(55, 155);

        Group root = new Group();

        root.getChildren().addAll(napraviKameru(),napraviTorus(510,55,100,25)); //kupa(100, 20, 10));

        Scene scene = new Scene(root, 400, 400);
        scene.setCamera(kamera);
        primaryStage.setTitle("Hello World!");
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    private Group napraviKameru() {
        Group g = new Group();

        kamera = new PerspectiveCamera(true);
        kamera.setFarClip(1000);
        kamera.setTranslateZ(-800);

        PointLight lampa = new PointLight();
        lampa.setColor(Color.WHITE);
        lampa.setTranslateX(kamera.getTranslateX());
        lampa.setTranslateY(kamera.getTranslateY());
        lampa.setTranslateZ(kamera.getTranslateZ());
        Translate t = new Translate(0, 0, -800);
        Rotate rx = new Rotate();
        rx.setAxis(Rotate.X_AXIS);
        rx.setAngle(-70);
        g.getChildren().addAll(kamera, lampa);

        g.getTransforms().addAll(rx);
        return g;

        //kamera.getTransforms().addAll(rx);
    }

    private MeshView napraviPovrsinu(int r, int n) {

        float[] temena = new float[3 * (n + 1)];
        temena[0] = 0f;
        temena[1] = 0f;
        temena[2] = 0f;
        int it = 3;//indeks temena iv

        double delatUgao = 2 * Math.PI / n;
        double ugao = 0.0;
        for (int i = 0; i < n; i++) {

            temena[it++] = (float) (r * Math.cos(ugao)); //x
            temena[it++] = 0f; //z
            temena[it++] = (float) (r * Math.sin(ugao)); //y

            ugao += delatUgao;
        }

        float[] tekstura = {
            0.1f, 0.5f, //0 crvena
            0.3f, 0.5f, //1 zelena
            0.5f, 0.5f, //2 plava
            0.7f, 0.5f, //3 zuta
            0.9f, 0.5f //4 nar
        };

        int is = 0, ib = 0;
        it = 1;
        int[] stranice = new int[12 * n];

        for (int i = 0; i < n; i++) {
            //lice
            stranice[is++] = 0;                                          stranice[is++] = ib;
            stranice[is++] = it;                                         stranice[is++] = ib;
            stranice[is++] = (it + 1) % (n + 1) != 0 ? it + 1 : 1;       stranice[is++] = ib;
            //nalicje
            stranice[is++] = 0;
            stranice[is++] = ib;
            stranice[is++] = (it + 1) % (n + 1) != 0 ? it + 1 : 1;
            stranice[is++] = ib;
            stranice[is++] = it;
            stranice[is++] = ib;
            it++;
        }

        TriangleMesh mreza = new TriangleMesh();
        mreza.getPoints().addAll(temena);
        mreza.getTexCoords().addAll(tekstura);
        mreza.getFaces().addAll(stranice);

        MeshView mv = new MeshView();
        mv.setMesh(mreza);

        PhongMaterial mat = new PhongMaterial();
        mat.setDiffuseColor(Color.BLUE);
        mat.setSpecularPower(150);
        mv.setDrawMode(DrawMode.LINE);
        //mv.setMaterial(mat);
        return mv;
    }

    private MeshView napraviTorus(int nU, int nP, float R, float r) {
        float[] temena = new float[nP * nU * 3];
        int[] stranice = new int[nP * nU * 12];

        int iv = 0, it = 0, is = 0;
        int p0, p1, p2, p3;
        int p;
        int t0 = 0, t1 = 0, t2 = 0, t3 = 0;

        for (int ip = 0; ip < nP; ip++) {
            float fi = ip * 2.0f * (float) Math.PI / nP;
            for (int iu = 0; iu < nU; iu++) {
                float o = iu * 2.0f * (float) Math.PI / nU;
                temena[iv++] = (float) ((R + r * Math.cos(fi)) * Math.cos(o));
                temena[iv++] = (float) ((R + r * Math.cos(fi)) * Math.sin(o));
                temena[iv++] = (float) (r * Math.sin(fi));
            }
        }

        float[] tekstura = {
            0.1f, 0.5f, //0 crvena
            0.3f, 0.5f, //1 zelena
            0.5f, 0.5f, //2 plava
            0.7f, 0.5f, //3 zuta
            0.9f, 0.5f //4 nar
        };

        for (int i = 0; i < nP; i++) {
            for (int j = 0; j < nU; j++) {
                p0 = i * nU + j;
                p = p0 + 1;
                p1 = p % nU != 0 ? p : p - nU;

                p = p0 + nU;
                p2 = p < nP * nU ? p : j;

                p = p2 + 1;
                p3 = p % nU != 0 ? p : p - nU;
                t0 = t1 = t2 = t3 = it;

                //gt lice
                stranice[is++] = p0;
                stranice[is++] = t0;
                stranice[is++] = p1;
                stranice[is++] = t1;
                stranice[is++] = p2;
                stranice[is++] = t2;

                //donji trougao
                stranice[is++] = p3;
                stranice[is++] = t3;
                stranice[is++] = p2;
                stranice[is++] = t2;
                stranice[is++] = p1;
                stranice[is++] = t1;
            }
            it = (it + 1) % (tekstura.length / 2);
        }

        TriangleMesh mreza = new TriangleMesh();
        mreza.getPoints().addAll(temena);
        mreza.getTexCoords().addAll(tekstura);
        mreza.getFaces().addAll(stranice);

        MeshView mv = new MeshView();
        mv.setMesh(mreza);

        PhongMaterial mat = new PhongMaterial();
        mat.setDiffuseColor(Color.RED);
        mat.setSpecularPower(150);
        //mv.setDrawMode(DrawMode.LINE);
        mv.setMaterial(mat);
        return mv;
    }

    private MeshView kupa(float H,float R,int n){
        float temana[] =new float[(n+1)*3];
        temana[0] = 0; 
        temana[2] = 0;
        temana[1] = H;
        
        int it=3;
        double fi=0;
        double deltaFi= 2*Math.PI/n;
        
        for(int i=0;i<n;i++){
            temana[it++] = (float)(R*Math.cos(fi));
            temana[it++] = 0;
            temana[it++] = (float)(R*Math.sin(fi));
            
            fi += deltaFi;
        }
        
         float[] tekstura = {
            0.1f, 0.5f, //0 crvena
            0.3f, 0.5f, //1 zelena
            0.5f, 0.5f, //2 plava
            0.7f, 0.5f, //3 zuta
            0.9f, 0.5f //4 nar
        };
         
        int [] stranice = new int[12*n];
        
        int ib=0;
        int is=0;
        it=1;
        for(int i=0;i<n;i++){
            //str gornja
            stranice[is++] = 0;                                         stranice[is++] = ib;
            stranice[is++] = it;                                        stranice[is++] = ib;
            stranice[is++] = (it + 1) % (n + 1) != 0 ? it + 1 : 1;      stranice[is++] = ib;
            
            //str donja
            stranice[is++] = 0;                                         stranice[is++] = ib;
            stranice[is++] = (it + 1) % (n + 1) != 0 ? it + 1 : 1;      stranice[is++] = ib;
            stranice[is++] = it;                                        stranice[is++] = ib;
            it++;
        }
        
        
        TriangleMesh mreza = new TriangleMesh();
        mreza.getPoints().addAll(temana);
        mreza.getTexCoords().addAll(tekstura);
        mreza.getFaces().addAll(stranice);

        MeshView mv = new MeshView();
        mv.setMesh(mreza);

        PhongMaterial mat = new PhongMaterial();
        mat.setDiffuseColor(Color.RED);
        mat.setSpecularPower(150);
        //mv.setDrawMode(DrawMode.LINE);
        mv.setMaterial(mat);
        return mv;
    }
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }

}
